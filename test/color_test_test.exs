defmodule ColorTestTest do
  use ExUnit.Case

  doctest ColorTest

  test "converts from rgb to hsl" do
    rgb = ColorTest.RGB.new()
    assert ColorTest.convert(rgb, ColorTest.HSL) == ColorTest.HSL.new(1, 2, 3)
  end

  test "converts from hsl to rgb" do
    hsl = ColorTest.HSL.new()
    assert ColorTest.convert(hsl, ColorTest.RGB) == ColorTest.RGB.new(3, 2, 1)
  end

  test "converting from rgb to rgb doesn't do anything" do
    rgb = ColorTest.RGB.new(5, 6, 7)
    assert ColorTest.convert(rgb, ColorTest.RGB) == rgb
  end

  test "converting from hsl to hsl doesn't do anything" do
    hsl = ColorTest.HSL.new(6, 7, 8)
    assert ColorTest.convert(hsl, ColorTest.HSL) == hsl
  end

  test "convert from Pantone to hsl" do
    # There's no direct conversion from Pantone to HSL, so this is
    # supposed to convert through RGB
    pantone = ColorTest.Pantone.new(100)
    assert ColorTest.convert(pantone, ColorTest.HSL) == ColorTest.HSL.new(1, 2, 3)
  end
end
