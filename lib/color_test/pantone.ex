defmodule ColorTest.Pantone do
  defstruct n: 0

  def new() do
    %__MODULE__{}
  end

  def new(n) do
    %__MODULE__{n: n}
  end
end

defmodule ColorTest.Pantone.ColorTest.RGB do
  defstruct [:from, :to]

  defimpl ColorTest.Converter do
    def convert(_pantone) do
      IO.puts("pantone -> rgb")
      ColorTest.RGB.new(3, 2, 1)
    end
  end
end

defmodule ColorTest.RGB.ColorTest.Pantone do
  defstruct [:from, :to]

  defimpl ColorTest.Converter do
    def convert(_rgb) do
      IO.puts("rgb -> pantone")
      ColorTest.Pantone.new(200)
    end
  end
end

