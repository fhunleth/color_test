defprotocol ColorTest.Converter do
  @fallback_to_any true
  @doc """
  Handle a color conversion.
  """
  def convert(conversion)
end

defimpl ColorTest.Converter, for: Any do
  def convert(conversion) do
    IO.puts("Trying #{inspect conversion.from.__struct__}->RGB->#{inspect conversion.to}")
    rgb = ColorTest.convert(conversion.from, ColorTest.RGB)
    ColorTest.convert(rgb, conversion.to)
  end
end
