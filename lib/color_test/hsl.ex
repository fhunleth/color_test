defmodule ColorTest.HSL do
  defstruct h: 0,
            s: 0,
            l: 0

  def new() do
    %__MODULE__{}
  end

  def new(h, s, l) do
    %__MODULE__{h: h, s: s, l: l}
  end
end

defmodule ColorTest.HSL.ColorTest.RGB do
  defstruct [:from, :to]

  defimpl ColorTest.Converter do
    def convert(_hsl) do
      IO.puts("hsl -> rgb")
      ColorTest.RGB.new(3, 2, 1)
    end
  end
end

defmodule ColorTest.RGB.ColorTest.HSL do
  defstruct [:from, :to]

  defimpl ColorTest.Converter do
    def convert(_rgb) do
      IO.puts("rgb -> hsl")
      ColorTest.HSL.new(1, 2, 3)
    end
  end
end
