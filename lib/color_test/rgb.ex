defmodule ColorTest.RGB do
  defstruct r: 0,
            g: 0,
            b: 0

  def new() do
    %__MODULE__{}
  end

  def new(r, g, b) do
    %__MODULE__{r: r, g: g, b: b}
  end
end
