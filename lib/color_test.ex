defmodule ColorTest do
  @moduledoc """
  Documentation for ColorTest.
  """

  @doc """
  Convert colors

  ```
  iex> color1 = ColorTest.RGB.new(255, 0, 0)
  iex> ColorTest.convert(color1, ColorTest.HSL)
  %ColorTest.HSL{h: 1, l: 3, s: 2}
  """

  # Handle the case where the color doesn't need any conversion
  def convert(%{__struct__: color_space} = c, color_space), do: c

  # Handle other cases
  def convert(color, what) do
    convert_struct = Module.concat(color.__struct__, what)
    s = %{__struct__: convert_struct, from: color, to: what}
    ColorTest.Converter.convert(s)
  end
end
