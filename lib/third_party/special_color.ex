defmodule ThirdParty.SpecialColor do
  @moduledoc """
  The idea behind this module is to show that users of the library
  can create modules for their own colorspaces and still be able to
  convert between them and everything else.
  """
  defstruct h: 0,
            s: 0,
            l: 0

  def new() do
    %__MODULE__{}
  end

  def new(h, s, l) do
    %__MODULE__{h: h, s: s, l: l}
  end
end

defmodule ThirdParty.SpecialColor.ColorTest.RGB do
  defstruct [:from, :to]

  defimpl ColorTest.Converter do
    def convert(_hsl) do
      IO.puts("special_color -> rgb")
      ColorTest.RGB.new(3, 2, 1)
    end
  end
end

defmodule ColorTest.RGB.ThirdParty.SpecialColor do
  defstruct [:from, :to]

  defimpl ColorTest.Converter do
    def convert(_rgb) do
      IO.puts("rgb -> special_color")
      ThirdParty.SpecialColor.new(1, 2, 3)
    end
  end
end
